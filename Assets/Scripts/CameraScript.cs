using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public bool lookAtPlayer = true;
	public Transform target;
    public float xOffset = 0.0f;
    public float yOffset = 0.0f;
    public float zOffset = 5f;

    void Start() {
        
    }

    void Update() {
        if (lookAtPlayer) {
			transform.position = new Vector3(target.position.x + xOffset, 
                transform.position.y + yOffset, target.position.z + zOffset);
			transform.LookAt(target);
		}
    }
}
